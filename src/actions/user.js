import {UPDATE_USER_DATA_FROM_LOCAL_STORAGE} from "../constants/auth";
import axios from 'axios'

export function updateUserDataFromLocalStorage() {
  return dispatch => {
    dispatch({
      type: UPDATE_USER_DATA_FROM_LOCAL_STORAGE,
      payload: JSON.parse(localStorage.getItem('user'))
    })
  }
}

