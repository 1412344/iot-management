import axios from 'axios'
import {toast} from 'react-toastify'
import config from '../config'
import {browserHistory} from 'react-router'
import {
  ADD_DEVICE_TO_INTERACTING_LIST, CLEAR_CURRENT_SELECTED_INTERACT_DEVICE,
  FETCH_LIST_DEVICE_SUCCESS,
  REMOVE_DEVICE, SELECT_DEVICE_TO_CURRENT_INTERACT, UPDATE_LIST_ADMIN_MANAGE_DEVICE,
  UPDATE_LIST_BORROW_DEVICE,
  UPDATE_LIST_FREE_DEVICE
} from "../constants/device";

const DEVICE_API_BASE_URL = config.BASE_API_URL + 'device/'
const ALLOCATION_API_BASE_URL = config.BASE_API_URL + 'allocation/'
const ARDUINO_API_BASE_URL = config.BASE_API_URL + 'arduino/'


export function fetchListFreeDevices(uid, platform) {
  return dispatch => {
    axios
      .get(`${DEVICE_API_BASE_URL}get_devices`, {params: {user: uid, platform: platform}})
      .then(response => {
        dispatch(updateListFreeDevice({items: response.data}))
      })
      .catch(error => {
        if (error.response)
          toast.error(error.response.data.message)
        else
          toast.error(error.message)
      })
  }
}

export function fetchListBorrowDevice(uid) {
  return dispatch => {
    axios
      .get(`${ALLOCATION_API_BASE_URL}getMyBorrowingDevice`)
      .then(response => {
        dispatch(updateListBorrowDevice({items: response.data}))
      })
      .catch(error => {
        if (error.response)
          toast.error(error.response.data.message)
        else
          toast.error(error.message)
      })
  }
}

export function borrowDevice(deviceId, borrowTimes) {
  return dispatch => {
    return axios
      .post(`${ALLOCATION_API_BASE_URL}borrow`, {deviceId: deviceId, usingTime: borrowTimes})
      .then(response => {
        toast.success(response.data.message)
        dispatch(removeDevice(deviceId, 'free'))
      })
      .catch(error => {
        if (error.response)
          toast.error(error.response.data.message)
        else
          toast.error(error.message)
      })
  }
}

export function interactWithDevice(deviceId) {
  return dispatch => {
    dispatch(addDeviceToInteract(deviceId))
    dispatch(selectDeviceToInteract(deviceId))
    browserHistory.push(`/device/interact/`)
  }
}

export function deployCode(socketId, deviceId, code) {
  return dispatch => {
    return axios
      .post(`${ARDUINO_API_BASE_URL}deploy/code`, {socketId: socketId, deviceID: deviceId, code: code})
      .then(response => {
        toast.success(response.data.message)
      })
      .catch(error => {
        if (error.response)
          toast.error(error.response.data.message)
        else
          toast.error(error.message)
      })
  }
}

export function deployFile(formData) {
  return dispatch => {
    return axios
      .post(`${ARDUINO_API_BASE_URL}deploy/file`, formData, {
        headers: {
          'Content-Type': 'multipart/form-data'
        }
      })
      .then(response => {
        toast.success(response.data.message)
      })
      .catch(error => {
        if (error.response)
          toast.error(error.response.data.message)
        else
          toast.error(error.message)
      })
  }
}

export function revokeDevice(deviceId) {
  return dispatch => {
    return axios
      .post(`${ALLOCATION_API_BASE_URL}revoke`, {deviceID: deviceId})
      .then(response => {
        toast.success(response.data.message)
        dispatch(removeDevice(deviceId, 'borrow'))
        dispatch(removeDevice(deviceId, 'interact'))
      })
      .catch(error => {
        if (error.response)
          toast.error(error.response.data.message)
        else
          toast.error(error.message)
      })
  }
}

export function clearAllCurrentInteractSelectedDevice() {
  return dispatch => {
    dispatch(clearAllCurrentSelectedDevice())
  }
}

const selectDeviceToInteract = deviceId => ({
  type: SELECT_DEVICE_TO_CURRENT_INTERACT,
  payload: {deviceId: deviceId}
})

const addDeviceToInteract = (deviceId) => ({
  type: ADD_DEVICE_TO_INTERACTING_LIST,
  payload: {deviceId: deviceId}
})

export const removeDevice = (deviceId, type) => ({
  type: REMOVE_DEVICE,
  payload: {deviceId: deviceId, type: type}
})

const updateListBorrowDevice = items => ({
  type: UPDATE_LIST_BORROW_DEVICE,
  payload: items
})

const updateListFreeDevice = items => ({
  type: UPDATE_LIST_FREE_DEVICE,
  payload: items
})

const clearAllCurrentSelectedDevice = () => ({
  type: CLEAR_CURRENT_SELECTED_INTERACT_DEVICE
})