import {
  LOGOUT,
  SUBMIT_LOGIN_FORM,
  SUBMIT_LOGIN_FORM_ERROR,
  SUBMIT_LOGIN_FORM_SUCCESS,
  UPDATE_USER_TOKEN
} from "../constants/auth";
import config from "../config";
import axios from 'axios'
import {toast} from 'react-toastify'
import {browserHistory} from 'react-router'

const ACCOUNT_BASE_API_URL = config.BASE_API_URL + 'account/'

export function login(username, password) {
  return dispatch => {
    dispatch(submitLoginForm(username, password))
    delete axios.defaults.headers.common['x-api-token']
    return axios
      .post(config.BASE_API_URL + 'account/login', {username, password})
      .then(res => {
        toast.success("Login Success")
        dispatch(loginSuccess(res.data))
        browserHistory.push('/dashboard')
      })
      .catch((error) => {
        if (error.response)
          toast.error(error.response.data.message)
        else
          toast.error(error.message)
      })
  }
}

export function registration(username, email, password) {
  return dispatch => {
    return axios
      .post(`${ACCOUNT_BASE_API_URL}register`, {username, email, password})
      .then(response => {
        toast.success(response.data.message)
        browserHistory.push('/login')
      })
      .catch(error => {
        if (error.response)
          toast.error(error.response.data.message)
        else
          toast.error(error.message)
      })
  }
}

export function verifyAccessToken() {
  return dispatch => {
    return axios
      .get(`${config.BASE_URL}check-token`)
      .then(response => {
        if (response.data.code === 200){
          browserHistory.push('/dashboard')
        }

      })
      .catch(error => {
        if (error.response)
          toast.error(error.response.data.message)
        else
          toast.error(error.message)
        browserHistory.push('/logout')
      })
  }
}

export function logout() {
  return dispatch => {
    dispatch(removeAccessToken())
    browserHistory.push('/login')
  }
}

const removeAccessToken = () => ({
  type: LOGOUT
})

const updateToken = token => ({
  type: UPDATE_USER_TOKEN,
  payload: token
})

const submitLoginForm = (username, password) => ({
  type: SUBMIT_LOGIN_FORM,
  payload: {
    username: username,
    password: password
  }
})

const loginSuccess = (data) => {
  axios.defaults.headers.common['x-api-token'] = data.access_token
  localStorage.setItem('user', JSON.stringify(data.userInfo))
  localStorage.setItem('token', data.access_token)
  return {
    type: SUBMIT_LOGIN_FORM_SUCCESS,
    payload: data
  }
}

const loginFail = (error) => ({
  type: SUBMIT_LOGIN_FORM_ERROR,
  payload: error
})