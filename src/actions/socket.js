import {DEPLOY_CODE_ARDUINO_TO_IO_SERVER, RECEIVE_DEPLOYED_CODE_DATA, UPDATE_SOCKET_ID} from "../constants/socket";
import {toast} from 'react-toastify'

export function deployCodeToIo(socket, deviceId, code) {
  return dispatch => {
    socket.emit('deploy_code', {code: code, deviceId: deviceId})
  }
}

export function updateSocketIdFromServer(socketId) {
  return dispatch => {
    dispatch(updateSocketId(socketId))
  }
}

export function receiveDeployedCodeResult(result) {
  return dispatch => {
    if (!result.flag)
      dispatch(receivedData(result.data, result.deviceId))
    else {
      if(result.code === 0)
        toast.success(`DEVICE: ${result.deviceId} RUN SUCCESS`)
    }
  }
}

const receivedData = (data, deviceId) => ({
  type: RECEIVE_DEPLOYED_CODE_DATA,
  payload: {data: data, deviceId: deviceId}
})

const deployCode = (deviceId, code) => ({
  type: DEPLOY_CODE_ARDUINO_TO_IO_SERVER,
  payload: {code: code, deviceId: deviceId}
})

const updateSocketId = socketId => ({
  tpye: UPDATE_SOCKET_ID,
  payload: {socketId: socketId}
})