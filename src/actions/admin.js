import {
  ADD_DEVICE_ITEM_TO_LIST_MANAGE_DEVICE,
  REMOVE_DEVICE_ITEM_FROM_LIST_MANAGE_DEVICE,
  UPDATE_LIST_ADMIN_MANAGE_DEVICE
} from "../constants/admin";
import axios from "axios/index";
import {toast} from "react-toastify";
import config from "../config";

const DEVICE_API_BASE_URL = config.BASE_API_URL + 'device/'

export function fetchAllDeviceForAdmin() {
  return dispatch => {
    axios.get(`${DEVICE_API_BASE_URL}get_devices`, {params: {}})
      .then(response => {
        dispatch(updateListAdminDeviceManage({items: response.data}))
      })
      .catch(error => {
        if (error.response)
          toast.error(error.response.data.message)
        else
          toast.error(error.message)
      })
  }
}

export function updateDeviceInfor(data) {
  return dispatch => {
    return axios
      .post(`${DEVICE_API_BASE_URL}updateInfo`, data)
      .then(response => {
        toast.success(response.data.message)
      })
      .catch(error => {
        if (error.response)
          toast.error(error.response.data.message)
        else
          toast.error(error.message)
      })
  }
}

export function addNewDevice(data) {
  return dispatch => {
    return axios
      .post(`${DEVICE_API_BASE_URL}create`, data)
      .then(response => {
        toast.success(response.data.message)
        dispatch(addDeviceItemToListManageDevice(response.data.deviceInfo))
        window.alert(`NEW DEVICE PASSWORD: ${response.data.deviceInfo.password}`)
      })
      .catch(error => {
        if (error.response)
          toast.error(error.response.data.message)
        else
          toast.error(error.message)
      })
  }
}

export function deleteDevice(deviceId) {
  return dispatch => {
    return axios
      .post(`${DEVICE_API_BASE_URL}delete`, {deviceId: deviceId})
      .then(response => {
        toast.success(response.data.message)
        dispatch(removeDeviceItemFromListManageDevice(deviceId))
      })
      .catch(error => {
        if (error.response)
          toast.error(error.response.data.message)
        else
          toast.error(error.message)
      })
  }
}

const removeDeviceItemFromListManageDevice = deviceId => ({
  type: REMOVE_DEVICE_ITEM_FROM_LIST_MANAGE_DEVICE,
  payload: {deviceId: deviceId}
})

const addDeviceItemToListManageDevice = item => ({
  type: ADD_DEVICE_ITEM_TO_LIST_MANAGE_DEVICE,
  payload: {item: item}
})

const updateListAdminDeviceManage = items => ({
  type: UPDATE_LIST_ADMIN_MANAGE_DEVICE,
  payload: items
})