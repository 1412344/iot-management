import axios from 'axios'
import config from '../config'
import {toast} from 'react-toastify'
import {FETCH_LIST_ACCOUNT_SUCCESS, UPDATE_USER_ACCOUNT_INFORMATION} from "../constants/constant.account";
import {UPDATE_LIST_ADMIN_ACCOUNT} from "../constants/admin";

const ACCOUNT_BASE_URL = config.BASE_API_URL + 'account/'

export function fetchAllAccount() {
  return dispatch => {
    axios.post(`${ACCOUNT_BASE_URL}get_users`, {})
      .then(response => {
        dispatch(updateListAdminAccount(response.data))
      })
      .catch(error => {
        if (error.response)
          toast.error(error.response.data.message)
        else
          toast.error(error.message)
      })
  }
}

export function updateAccountInfo(data) {
  return dispatch => {
    return axios.post(`${ACCOUNT_BASE_URL}update`, data)
      .then(response => {
        toast.success(response.data.message)
        // dispatch(updateUserInfo({accounts: response.data}))
      })
      .catch(error => {
        if (error.response)
          toast.error(error.response.data.message)
        else
          toast.error(error.message)
      })
  }
}

export function fetchUserInfo(uid) {
  return dispatch => {
    return axios
      .get(`${ACCOUNT_BASE_URL}info`, {params: {uid: uid}})
      .then(response => {
        dispatch(updateUserInfo(response.data))
      })
      .catch(error => {
        if (error.response)
          toast.error(error.response.data.message)
        else
          toast.error(error.message)
      })
  }
}

export function disableAccount(email) {
  return dispatch => {
    return axios
      .post(`${ACCOUNT_BASE_URL}disable`, {email: email})
      .then((response=> {
        console.log(response.data)
        toast.success(response.data.message)
      }))
      .catch(error=>{
        if (error.response)
          toast.error(error.response.data.message)
        else
          toast.error(error.message)
      })
  }
}
export function banAccount(email) {
  return dispatch => {
    return axios
      .post(`${ACCOUNT_BASE_URL}ban`, {email: email})
      .then((response=> {
        console.log(response.data)
        toast.success(response.data.message)
      }))
      .catch(error=>{
        if (error.response)
          toast.error(error.response.data.message)
        else
          toast.error(error.message)
      })
  }
}

export function editRole(accountId, role) {
  return dispatch => {
    return axios
      .post(`${ACCOUNT_BASE_URL}set_role`, {roleName: role, accountId: accountId})
      .then(response=>{
        console.log(response.data)
        toast.success(response.data.message)
      })
      .catch(error=>{
        if (error.response)
          toast.error(error.response.data.message)
        else
          toast.error(error.message)
      })
  }
}

const updateUserInfo = user => ({
  type: UPDATE_USER_ACCOUNT_INFORMATION,
  payload: user
})

const updateListAccount = accounts => ({
  type: FETCH_LIST_ACCOUNT_SUCCESS,
  payload: accounts
})

const updateListAdminAccount = accounts => ({
  type: UPDATE_LIST_ADMIN_ACCOUNT,
  payload: {items: accounts}
})