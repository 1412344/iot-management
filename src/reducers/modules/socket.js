import io from 'socket.io-client'
import config from '../../config'

const initState = {
  connection: io.connect(config.BASE_URL, {
    path: '/socket.io'
  })
}

const socket = (state = initState, action) => {
  switch (action.type) {
    default:
      return state
  }
}

export default socket