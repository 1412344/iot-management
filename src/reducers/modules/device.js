import {
  ADD_DEVICE_TO_INTERACTING_LIST, CLEAR_CURRENT_SELECTED_INTERACT_DEVICE,
  FETCH_LIST_DEVICE_SUCCESS,
  REMOVE_DEVICE, SELECT_DEVICE_TO_CURRENT_INTERACT,
  UPDATE_LIST_BORROW_DEVICE,
  UPDATE_LIST_FREE_DEVICE
} from "../../constants/device";
import {RECEIVE_DEPLOYED_CODE_DATA} from "../../constants/socket";

const initState = {
  borrow: {
    items: []
  },
  free: {
    items: []
  },
  interact: {
    items: [],
    result: [],
    current: null
  },
  manage: {
    users: {
      items: []
    },
    device: {
      items: []
    }
  }
}

const device = (state = initState, action) => {
  switch (action.type) {
    case FETCH_LIST_DEVICE_SUCCESS:
      return {
        ...state,
        items: action.payload.items
      }
    case UPDATE_LIST_BORROW_DEVICE:
      return {
        ...state,
        borrow: {
          ...state.borrow,
          items: action.payload.items
        }
      }
    case UPDATE_LIST_FREE_DEVICE:
      return {
        ...state,
        free: {
          ...state.free,
          items: action.payload.items
        }
      }
    case REMOVE_DEVICE:
      switch (action.payload.type) {
        case 'free':
          return {
            ...state,
            free: {
              ...state.free,
              items: state.free.items.filter(i => i._id !== action.payload.deviceId)
            }
          }
        case 'borrow':
          return {
            ...state,
            borrow: {
              ...state.borrow,
              items: state.borrow.items.filter(i => i._id !== action.payload.deviceId)
            }
          }
        case 'interact':
          return {
            ...state,
            interact: {
              ...state.interact,
              items: state.interact.items.filter(i => i !== action.payload.deviceId),
              result: state.interact.result.filter(i => i.deviceId !== action.payload.deviceId)
            }
          }
      }
    case ADD_DEVICE_TO_INTERACTING_LIST:
      if (!state.interact.items.find(i => i === action.payload.deviceId))
        return {
          ...state,
          interact: {
            ...state.interact,
            items: [
              ...state.interact.items,
              action.payload.deviceId
            ],
            result: [
              ...state.interact.result,
              {
                deviceId: action.payload.deviceId,
                result: ''
              }
            ]
          }
        }
      else
        return state
    case RECEIVE_DEPLOYED_CODE_DATA:
      return {
        ...state,
        interact: {
          ...state.interact,
          result: state.interact.result.map((item, index) => {
            if (item.deviceId === action.payload.deviceId) {
              return {
                ...item,
                result: item.result.concat(action.payload.data)
              }
            }
            else
              return {
                ...item
              }
          })
        }
      }
    case SELECT_DEVICE_TO_CURRENT_INTERACT:
      return {
        ...state,
        interact: {
          ...state.interact,
          current: action.payload.deviceId
        }
      }
    case CLEAR_CURRENT_SELECTED_INTERACT_DEVICE:
      return {
        ...state,
        interact: {
          ...state.interact,
          current: null
        }
      }
    default:
      return state
  }
}

export default device