import {FETCH_LIST_ACCOUNT_SUCCESS} from "../../constants/constant.account";

const initState = {
  items: [],
}

const account = (state = initState, action) => {
  switch (action.type) {
    case FETCH_LIST_ACCOUNT_SUCCESS:
      return {
        ...state,
        items: action.payload.accounts
      }
    default:
      return state
  }
}

export default account