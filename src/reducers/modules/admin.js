import {
  ADD_DEVICE_ITEM_TO_LIST_MANAGE_DEVICE,
  REMOVE_DEVICE_ITEM_FROM_LIST_MANAGE_DEVICE, UPDATE_LIST_ADMIN_ACCOUNT,
  UPDATE_LIST_ADMIN_MANAGE_DEVICE
} from "../../constants/admin";

const initState = {
  device: {
    items: []
  },
  account: {
    items: []
  }
}

const admin = (state = initState, action) => {
  switch (action.type) {
    case UPDATE_LIST_ADMIN_ACCOUNT:
      return {
        ...state,
        account:{
          ...state.account,
          items: action.payload.items
        }
      }
    case UPDATE_LIST_ADMIN_MANAGE_DEVICE:
      return {
        ...state,
        device: {
          ...state.device,
          items: action.payload.items
        }
      }
    case ADD_DEVICE_ITEM_TO_LIST_MANAGE_DEVICE:
      return {
        ...state,
        device: {
          ...state.device,
          items: [
            action.payload.item,
            ...state.device.items
          ]
        }
      }
    case REMOVE_DEVICE_ITEM_FROM_LIST_MANAGE_DEVICE:
      return {
        ...state,
        device: {
          ...state.device,
          items: state.device.items.filter(i => i._id !== action.payload.deviceId)
        }
      }
    default:
      return state
  }
}

export default admin
