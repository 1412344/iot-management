import {
    LOGOUT,
    REGISTER_SUCCESS,
    SUBMIT_LOGIN_FORM,
    SUBMIT_LOGIN_FORM_ERROR,
    SUBMIT_LOGIN_FORM_SUCCESS, UPDATE_USER_DATA_FROM_LOCAL_STORAGE,
    UPDATE_USER_TOKEN
} from "../../constants/auth";
import {UPDATE_USER_ACCOUNT_INFORMATION} from "../../constants/constant.account";

const initState = {
  user: {},
  token: '',
  state: 0, // 0 - NONE; 1 - IN LOGIN; 2 - LOGIN SUCCESS; 3 - IN REGISTER; 4 - REGISTER SUCCESS
  error: ''
};

const auth = (state = initState, action) => {
  switch (action.type) {
    case SUBMIT_LOGIN_FORM:
      return {
        ...state,
        isLoggingIn: true
      }
    case SUBMIT_LOGIN_FORM_SUCCESS:
      return {
        ...state,
        user: action.payload.userInfo,
        token: action.payload.access_token,
        isLoggingIn: true
      }
    case SUBMIT_LOGIN_FORM_ERROR:
      return {
        ...state,
        error: action.payload.error
      }
    case UPDATE_USER_TOKEN:
      return {
        ...state,
        token: action.payload.access_token
      }
    case UPDATE_USER_DATA_FROM_LOCAL_STORAGE:
      return {
        ...state,
        token: localStorage.getItem('token'),
        user:JSON.parse(localStorage.getItem('user'))
      }
    case REGISTER_SUCCESS:
      return {
        ...state,
        state: 4
      }
    case UPDATE_USER_ACCOUNT_INFORMATION:
      return {
        ...state,
        user: action.payload
      }
      case LOGOUT:
        return {
            ...state,
            token: null,
            user: {},
            state: 0
        }
    default:
      return state
  }
};

export default auth