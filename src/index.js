import React from 'react';
import {render} from 'react-dom'
import thunkMiddleware from 'redux-thunk'
import {applyMiddleware, createStore, compose} from "redux";
import rootReducer from './reducers'
import {Provider} from "react-redux";
import {Route, Router, browserHistory} from "react-router";
import {syncHistoryWithStore} from "react-router-redux";
import axios from 'axios'

import './assets/css/icons/icomoon/styles.css'
import './assets/css/bootstrap.css'
import './assets/css/core.css'
import './assets/css/components.css'
import './assets/css/my-style.css'

//IMPORT REAT-TOAST CSS
import 'react-toastify/dist/ReactToastify.css'

//IMPORT REACT-TABLE CSS
import "react-table/react-table.css"

//IMPORT REACT_SELECT CSS
import 'react-select/dist/react-select.min';

//IMPORT CONTAINERS
import Home from './components/home'
import Login from './components/Auth/login'
import Logout from './components/Auth/logout'
import Register from './components/Auth/register'
import AccountManagement from './components/Admin/component.admin.account'
import Devicemanagement from './components/Admin/component.admin.device'
import User from './components/user'
import Device from './components/Device/component.device'
import DeviceInteract from './components/Device/device.interact'
import AssignDevice from './components/Device/component.assign'
import {verifyAccessToken} from "./actions/auth";

//CONFIG AXIOS
const token = localStorage.getItem('token')
if (token)
    axios.defaults.headers.common['x-api-token'] = token;

const store = createStore(
  rootReducer,
  {
    auth: {
      user:JSON.parse(localStorage.getItem('user'))
    }
  },
  compose(
    applyMiddleware(thunkMiddleware),
    window.devToolsExtension ? window.devToolsExtension() : f => f
  )
)

store.dispatch(verifyAccessToken())

// const store = require('./configStore')
const history = syncHistoryWithStore(browserHistory, store);

render(
  <Provider store={store}>
    <Router history={history}>
      <Route path='/dashboard' component={Home}></Route>
      <Route path='/login' component={Login}></Route>
      <Route path='/logout' component={Logout}></Route>
      <Route path='/register' component={Register}></Route>
      <Route path='/' component={Login}></Route>
      <Route path='/admin/account' component={AccountManagement}></Route>
      <Route path='/admin/device' component={Devicemanagement}></Route>
      <Route path='/user' component={User}></Route>
      <Route path='/device' component={Device}></Route>
      <Route path='/device/manage' component={Device}></Route>
      <Route path='/device/assign' component={AssignDevice}></Route>
      <Route path='/device/interact' component={DeviceInteract}></Route>
    </Router>
  </Provider>,
  document.getElementById('root')
)