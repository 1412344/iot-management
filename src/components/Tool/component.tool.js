import React, {Component} from 'react'
import Header from '../Layout/header'
import SideBar from '../Layout/sidebar'
import {connect} from "react-redux";
import ReactTable from 'react-table'
import {fetchListProfileByToolIdAndUserId, saveProfile} from "../../actions/profile";
import {fetchLitPayloadToolByToolId, fetchToolTemplate} from "../../actions/tool";
import {toast} from 'react-toastify'
import Creatable from 'react-select/lib/Creatable'
import Select from 'react-select'
import Modal from 'react-modal'

class Tool extends Component {
  constructor(props) {
    super(props)
    this.state = {
      table: {
        tool: {
          expand: [],
          columns: [
            {
              expander: true,
              Header: () => <strong>Expand</strong>,
              width: 65,
              Expander: ({isExpanded, ...rest}) => {
                return <div onClick={() => this.handleExpand(rest)}>
                  {isExpanded
                    ? <span>&#x2299;</span>
                    : <span>&#x2295;</span>}
                </div>
              }
              ,
              style: {
                cursor: "pointer",
                fontSize: 25,
                padding: "0",
                textAlign: "center",
                userSelect: "none"
              }
            },
            {
              Header: 'Name',
              accessor: 'name',
            },
            {
              Header: 'Actions',
              width: 650,
              Cell: row => {
                return <div className="btn-group text-center">
                  <button type="button"
                          className="btn border-slate text-slate-800 btn-labeled"
                          title="Edit Profile"
                          onClick={() => this.handleOpenAndEditModal(row)}>
                    <b><i className="icon-wrench2"></i></b>EDIT
                  </button>
                  <button type="button"
                          className="btn border-slate text-slate-800 btn-labeled"
                          title="Edit Profile"
                          onClick={() => this.handleEditRow(row)}>
                    <b><i className="icon-eye"></i></b>QUICK VIEW
                  </button>
                  <button type="button" className="btn border-slate text-slate-800 btn-labeled"
                          title="Execute Profile"
                          onClick={() => this.handleExecuteProfile(row.original._id)}>
                    <b><i className="icon-bug2"></i></b>RUN
                  </button>
                  <button type="button"
                          className="btn border-slate text-slate-800 btn-labeled"
                          title="View Result">
                    <b><i className="icon-task"></i></b>RESULT
                  </button>
                  <button type="button" className="btn border-slate text-slate-800 btn-labeled"
                          title="Delete Profile">
                    <b><i className="icon-cross2"></i></b>DELETE
                  </button>
                </div>
              }
            }
          ]
        },
        profile: {
          columns: [
            {
              Header: 'Field',
              width: 200,
              Cell: row => {
                return <div
                  className="text-center">{row.original.alias ? row.original.alias : row.original.name}</div>
              }
            },
            {
              Header: 'Value',
              sortable: false,
              Cell: row => {
                switch (row.original.type) {
                  case 'dropbox':
                    return <Creatable
                      value={{value: row.original.value, label: row.original.value}}
                      isDisabled={this.state.modal.mode === 0}
                      options={row.original.values}
                      onChange={(e) => this.handleOnChangeDropboxValue(e, row)}/>
                  case 'file': {
                    switch (this.state.modal.mode) {
                      case 0:
                        return <div className="text-center">
                          {row.original.value.map(item => <p>{item.originalname}</p>)}
                        </div>
                      case 1:
                        return <div>
                          <Select
                            isMulti={true}
                            clearable={true}
                            isSearchable={true}
                            options={this.props.payloads}
                            onChange={(e) => this.handleOnChangePayload(e, row)}/>
                          <input type='file' multiple={true} onChange={(e) => this.handleOnChangeFiles(e)}/>
                        </div>
                      case 2:
                        return <div>
                          <Select
                            isMulti={true}
                            clearable={true}
                            isSearchable={true}
                            options={this.props.payloads}
                            onChange={(e) => this.handleOnChangePayload(e, row)}/>
                        </div>
                    }
                  }
                  case 'text':
                    return <input
                      disabled={this.state.modal.mode === 0}
                      className="form-control"
                      value={row.original.value}
                      onChange={(e) => this.handleOnChangeTextValue(e, row)}/>
                }
              }
            },
          ]
        },
      },
      modal: {
        mode: 0, //0 - none; 1 - edit; 2 - instant run
        isOpen: false,
        data: {
          profile: [],
          profileId: '',
          profileName: '',
          files: []
        },
      }
    }
  }

  shouldComponentUpdate(nextProps, nextState) {
    if (!Object.values(nextProps.template).length) {
      return false
    }
    return true
  }

  static getDerivedStateFromProps(props, state) {
    if (Object.values(props.template).length) {
      if (!state.table.tool.expand.length) {
        let tmpState = state
        tmpState.table.tool.expand.length = Object.values(props.template).length
        tmpState.table.tool.expand.fill(false)
        return tmpState
      }
    }
    return null
  }

  componentDidMount() {
    //FETCH LIST PROFILE
    this.props.dispatch(fetchListProfileByToolIdAndUserId(this.props.params.id))
    //FETCH TOOL TEMPLATE TO GET PROFILE TABLE COLUMNS
    this.props.dispatch(fetchToolTemplate(this.props.params.id))
    //FETCH LIST PAYLOAD
    this.props.dispatch(fetchLitPayloadToolByToolId(this.props.params.id))
  }

  render() {
    const {profiles, template} = this.props
    return (
      <div>
        <div>
          <div className="navbar navbar-default header-highlight  sticky">
            <Header/>
          </div>
          <div className="page-container data">
            <div className="page-content">
              <SideBar/>
              <div className="content-wrapper">
                <div className="content">
                  <div className="panel-group collapsible-sortable content-group-lg ui-sortable">
                    <div className="panel">
                      <div className="panel-heading bg-slate-800 mb-15">
                        <div className="heading-elements">
                          <button className="btn btn-flat" onClick={() => this.handleTest()}>TEST BTN</button>
                          <button type="button"
                                  className="btn border-white text-white-800 btn-flat dropdown-toggle legitRipple"
                                  onClick={() => this.handleCreateInstantRun()}>
                            INSTANT
                          </button>
                          <button type="button"
                                  className="btn border-white text-white-800 btn-flat dropdown-toggle legitRipple"
                                  onClick={() => this.handleAddNewProfile()}>
                            ADD NEW <span className="icon-plus2"></span></button>
                        </div>
                      </div>
                      <div className="panel-body">
                        <ReactTable
                          data={profiles}
                          columns={this.state.table.tool.columns}
                          expanded={this.state.table.tool.expand}
                          className="-highlight"
                          defaultPageSize={5}
                          SubComponent={row => {
                            return (
                              <ReactTable
                                defaultPageSize={this.props.template.length}
                                data={Object.values(template).length ? template.map(item => {
                                  if (item.name === 'file')
                                    return Object.assign({}, item, {value: row.original.attachFiles})
                                  return Object.assign({}, item, {value: row.original.optionals[`${item.name}`]})
                                }) : []}
                                showPagination={false}
                                columns={this.state.table.profile.columns}/>
                            )
                          }}/>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <Modal
          isOpen={this.state.modal.isOpen}
          onRequestClose={() => this.handleCloseModal()}
          ariaHideApp={false}
          shouldCloseOnEsc={true}
          className='modal-dialog modal-sm'>

          <div className="modal-content">
            <div className="modal-header">
              <h3>Profile {this.state.modal.data.profile.name}</h3>
              <button className="close icon-x"
                      type="button"
                      data-dismiss="modal"
                      onClick={() => this.handleCloseModal()}/>
            </div>
            <div className="modal-body">
              <div className="row mb-10">
                <ReactTable
                  defaultPageSize={this.props.template.length}
                  data={this.state.modal.data.profile}
                  showPagination={false}
                  columns={this.state.table.profile.columns}/>
              </div>
            </div>
            {
              this.state.modal.mode > 0 ?
                (
                  this.state.modal.mode > 1 ?
                    (
                      <div className="modal-footer">
                        <button type="button" className="btn btn-danger btn-xlg"
                                onClick={() => this.handleExecuteProfile(this.state.modal.data.profileId)}>EXECUTE
                        </button>
                        <button type="button" className="btn btn-primary"
                                data-dismiss="modal"
                                onClick={() => this.handleCloseModal()}>CLOSE
                        </button>
                      </div>
                    )
                    :
                    (
                      <div className="modal-footer">
                        <button type="button" className="btn btn-danger btn-xlg"
                                onClick={() => this.handleOnSaveProfile()}>SAVE
                        </button>
                        <button type="button" className="btn btn-warning btn-xlg"
                                onClick={() => this.handleOnCancelEditModal()}>CANCEL
                        </button>
                        <button type="button" className="btn btn-primary"
                                data-dismiss="modal"
                                onClick={() => this.handleCloseModal()}>CLOSE
                        </button>
                      </div>
                    )
                )
                :
                (
                  <div className="modal-footer">
                    <button type="button" className="btn btn-default btn-xlg"
                            onClick={this.handleSwitchToEditMode.bind(this)}>
                      EDIT
                    </button>
                    <button type="button" className="btn btn-primary"
                            data-dismiss="modal"
                            onClick={this.handleCloseModal.bind(this)}>CLOSE
                    </button>
                  </div>
                )
            }
          </div>
        </Modal>
      </div>
    )
  }

  handleOnSaveProfile() {
    let formData = new FormData()
    formData.append('tool', this.props.params.id)
    this.state.modal.data.profile.map(i => {
      formData.append(i.name, i.value)
    })
    this.state.modal.data.files.map(i => {
      formData.append('files[]', i)
    })
    this.props.dispatch(saveProfile(this.state.modal.data.profileId, formData))
  }

  handleOnCancelEditModal() {
    let tmpState = this.state
    tmpState.modal.mode = 0
    this.setState(tmpState)
  }

  handleCloseModal() {
    let tmpState = this.state
    tmpState.modal.isOpen = false
    this.setState(tmpState)
  }

  handleSwitchToEditMode() {
    let tmpState = this.state
    tmpState.modal.mode = 1
    this.setState(tmpState)
  }

  handleAddNewProfile() {
    console.log('ADD NEW')
  }

  handleEditRow(row) {
    let tmpState = this.state
    tmpState.table.tool.expand[row.index] = !this.state.table.tool.expand[row.index]
    // tmpState.table.tool.edit[row.index] = true
    this.setState(tmpState)
  }

  handleExpand(row) {
    let tmpState = this.state
    tmpState.table.tool.expand[row.index] = !tmpState.table.tool.expand[row.index]
    this.setState(tmpState)
  }

  handleTest() {
    // let tmpState = this.state
    // tmpState.table.tool.edit.fill(true)
    // this.setState(tmpState)
  }

  handleOnChangeDropboxValue(e, row) {
    let tmpState = this.state
    tmpState.modal.data.profile[row.index].value = e.value
    this.setState(tmpState)
  }

  handleOnChangeTextValue(e, row) {
    let tmpState = this.state
    tmpState.modal.data.profile[row.index].value = e.target.value
    this.setState(tmpState)
  }

  handleOnChangeFiles(e) {
    let tmpState = this.state
    tmpState.modal.data.files = e.target.files
    this.setState(tmpState)
  }

  handleOnChangePayload(e, row) {
    let tmpState = this.state
    tmpState.modal.data.files = e
    this.setState(tmpState)
  }

  handleOpenAndEditModal(row) {
    let obj = []
    this.props.template.map(i => {
      if (i.type === 'file')
        obj.push(Object.assign({}, i, {value: row.original.attachFiles}))
      obj.push(Object.assign({}, i, {value: row.original.optionals[i.name]}))
    })
    this.setState({
      ...this.state,
      modal: {
        ...this.state.modal,
        mode: 1,
        isOpen: true,
        isEdit: true,
        data: {
          ...this.state.modal.data,
          profile: obj,
          profileId: row.original._id,
          profileName: row.original.name
        }
      }
    })
  }

  handleCloseModal() {
    this.setState({
      ...this.state,
      modal: {
        ...this.state.modal,
        isOpen: false
      }
    })
  }

  handleCreateInstantRun() {
    let tmpState = this.state
    tmpState.modal.mode = 2
    tmpState.modal.isOpen = true
    tmpState.modal.data.profile = this.props.template
    tmpState.modal.data.profile.map(i => {
      i.value = i.default
    })
    this.setState(tmpState)
  }

  handleExecuteProfile(profileId) {
    switch (this.state.modal.mode) {
      case 1:
        this.props.dispatch(execute(this.props.params.id, profileId))
        break
      case 2:
        this.props.dispatch(instantExecute(this.props.params.id, Object.assign({},
          this.state.modal.data.profile,
          {file: this.state.modal.data.files})))
        break
    }
    // this.props.dispatch(execute(this.props.params.id, Object.assign({}, this.state.modal.data.profile, this.state.modal.data.files)))
  }
}

const mapStateToProps = state => {
  // console.log('MAP STATE TO PROPS')
  return {
    profiles: state.profile.profiles,
    template: state.tool.template,
    tools: state.tool.tools,
    payloads: state.tool.payloads
  }
}

export default connect(mapStateToProps)(Tool)