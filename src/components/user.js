import {connect} from "react-redux";
import React, {Component} from 'react'
import {toast} from 'react-toastify'

import Header from './Layout/header'
import SideBar from './Layout/sidebar'
import {updateUserDataFromLocalStorage} from "../actions/user";
import {fetchUserInfo, updateAccountInfo} from "../actions/action.account";

class User extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isEdit: false,
      user: props.user
    }
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      ...this.state,
      user: nextProps.user
    })
  }

  componentDidMount() {
    // this.props.dispatch(fetchUserInfo(this.props.user.uid))
  }

  render() {
    let {user} = this.props

    return (
      <div>
        <div className="navbar navbar-default header-highlight  sticky">
          <Header/>
        </div>
        <div className="page-container data">
          <div className="page-content">
            <SideBar/>
            <div className="content-wrapper">
              <div className="content">
                <div className="panel-group collapsible-sortable content-group-lg ui-sortable">
                  <div className="panel col-md-6 col-md-offset-3">
                    <div className="panel-body">
                      <div className="form-horizontal">
                        <div className="form-group">
                          <label className="col-md-3 control-label">Name</label>
                          <div className="col-md-9">
                            <input className="form-control"
                                   ref="displayName"
                                   value={this.state.user.displayName}
                                   disabled={!this.state.isEdit}
                                   onChange={(e) => this.handleOnChangeInput(e, 'displayName')}/>
                          </div>
                        </div>
                        <div className="form-group">
                          <label className="col-md-3 control-label">Email</label>
                          <div className="col-md-9">
                            <input className="form-control"
                                   value={this.state.user.email}
                                   disabled={true}/>
                          </div>
                        </div>
                        <div className="form-group">
                          <label className="col-md-3 control-label">Is Activated</label>
                          <div className="col-md-9">
                            <input type="checkbox" className="switchery"
                                   checked={this.state.user.isActivated}
                                   disabled={true}/>
                          </div>
                        </div>
                        <div className="form-group">
                          <label className="col-md-3 control-label">Class Name</label>
                          <div className="col-md-9">
                            <input className="form-control"
                                   ref="class_name"
                                   value={this.state.user.class_name}
                                   disabled={!this.state.isEdit}
                                   onChange={(e) => this.handleOnChangeInput(e, 'class_name')}/>
                          </div>
                        </div>
                        <div className="form-group">
                          <label className="col-md-3 control-label">Facility</label>
                          <div className="col-md-9">
                            <input className="form-control"
                                   ref="facility"
                                   value={this.state.user.facility}
                                   disabled={!this.state.isEdit}
                                   onChange={(e) => this.handleOnChangeInput(e, 'facility')}/>
                          </div>
                        </div>
                        <div className="form-group">
                          <label className="col-md-3 control-label">University</label>
                          <div className="col-md-9">
                            <input className="form-control"
                                   ref="university"
                                   value={this.state.user.university}
                                   disabled={!this.state.isEdit}
                                   onChange={(e) => this.handleOnChangeInput(e, 'university')}/>
                          </div>
                        </div>
                        <div className="form-group">
                          <label className="col-md-3 control-label">Phone</label>
                          <div className="col-md-9">
                            <input className="form-control"
                                   ref="phone"
                                   value={this.state.user.phone}
                                   disabled={!this.state.isEdit}
                                   onChange={(e) => this.handleOnChangeInput(e, 'phone')}/>
                          </div>
                        </div>
                        {this.state.isEdit ?
                          (<div className="form-group text-center">
                            <button className="btn btn-warning" onClick={() => this.handleOnSaveProfile()}>SAVE</button>
                            <button className="btn btn-default"
                                    onClick={() => this.handleOnCancelEditProfile()}>CANCEL
                            </button>
                          </div>)
                          :
                          (<div className="form-group text-center">
                            <button className="btn btn-primary" onClick={() => this.handleOnEditProfile()}>EDIT
                              PROFILE
                            </button>
                          </div>)}

                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }

  handleOnEditProfile() {
    this.setState({
      ...this.state,
      isEdit: true
    })
  }

  handleOnCancelEditProfile() {
    this.setState({
      ...this.state,
      isEdit: false
    })
  }

  handleOnChangeInput(e, field) {
    let tmpState = this.state
    tmpState.user[field] = e.target.value
    this.setState(tmpState)
  }

  handleOnSaveProfile() {
    let data = this.state.user
    data.uid = this.props.user._id
    this.props.dispatch(updateAccountInfo(data))
    this.setState({
      ...this.state,
      isEdit: false
    })
  }
}

const mapStateToProps = state => ({
  user: state.auth.user
})

export default connect(mapStateToProps)(User)