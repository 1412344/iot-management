import React, {Component} from 'react'

class ProgressBar extends Component {
  constructor(props) {
    super(props)
    console.log('PROGRESS BAR PROPS', props)
    const currentTime = new Date().getTime() / 1000

    this.state = {
      deviceId: props.deviceId,
      lasting: props.expiredAt.getTime() / 1000 - currentTime,
      width: `${((props.expiredAt.getTime() / 1000) - currentTime) / ((props.expiredAt.getTime() - props.startedAt.getTime()) / 1000) * 100}%`
    }

    this.timer = setInterval(() => {
      const currentTime = new Date().getTime() / 1000
      if (this.state.lasting >= 0)
        this.setState({
          lasting: (this.props.expiredAt.getTime() / 1000) - currentTime,
          width: `${((this.props.expiredAt.getTime() / 1000) - currentTime) / ((this.props.expiredAt.getTime() - this.props.startedAt.getTime()) / 1000) * 100}%`
        })
      else {
        clearInterval(this.timer)
        this.props.removeExpiredItem(this.state.deviceId)
      }
    }, 1000)
  }

  componentWillMount() {

  }

  render() {
    const lastingTime = this.state.lasting
    const hours = Math.floor(lastingTime / 3600)
    const minutes = Math.floor((lastingTime - hours * 3600) / 60)
    const seconds = Math.floor(lastingTime - hours * 3600 - minutes * 60)
    const label = (hours ? hours + 'h' : '') + (minutes ? minutes + 'm' : '') + (seconds ? seconds + 's' : '')
    return (
      <div className="progress">
        <div className="progress-bar bg-info" style={{width: this.state.width}}>
          <span>{label}</span>
        </div>
      </div>
    )
  }

  timer() {
    this.setState({
      lasting: new Date() - this.props.expiredAt
    })
  }
}

export default ProgressBar