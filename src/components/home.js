import React, {Component} from 'react';
import Header from "./Layout/header";
import SideBar from "./Layout/sidebar";
import {connect} from "react-redux";

class Home extends Component {
  constructor(props){
    super(props)
  }

  render(){
    return(
      <div>
        <div className="navbar navbar-default header-highlight  sticky">
          <Header/>
        </div>
        <div className="page-container data">
          <div className="page-content">
            <SideBar/>
            <div className="content-wrapper">
              <div className="content">
                <div className="panel-group collapsible-sortable content-group-lg ui-sortable">

                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

const mapStateToProps = state => ({

})

export default connect(mapStateToProps)(Home)