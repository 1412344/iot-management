import {connect} from "react-redux";
import React, {Component} from 'react'
import {receiveDeployedCodeResult, updateSocketIdFromServer} from "../actions/socket";

class Socket extends Component {
  constructor(props) {
    super(props)

    this.state = {}
  }

  componentWillMount() {

  }

  componentWillUnmount() {

  }

  componentDidMount() {
    if (!this.props.socket.connection._callbacks['$deploy/code'])
      this.props.socket.connection.on('deploy/code', data => {
        this.props.dispatch(receiveDeployedCodeResult(data))
      })
    if (!this.props.socket.connection._callbacks['$deploy/file'])
      this.props.socket.connection.on('deploy/file', data => {
        this.props.dispatch(receiveDeployedCodeResult(data))
      })
  }

  render() {
    return null
  }
}

const mapStateToProps = state => ({
  socket: state.socket
})

export default connect(mapStateToProps)(Socket)