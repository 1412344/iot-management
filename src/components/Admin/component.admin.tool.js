import {connect} from "react-redux";
import React, {Component} from 'react'
import ReactTable from 'react-table'
import Modal from 'react-modal'
import Creatable from 'react-select/lib/Creatable'
import Select from 'react-select'

import Header from '../Layout/header'
import SideBar from '../Layout/sidebar'
import {fetchListTool} from "../../actions/tool";


class ComponentAdminTool extends Component {
  constructor(props) {
    super(props)
    this.state = {
      table: {
        listTools: [
          {
            Header: 'Name',
            accessor: 'name'
          },
          {
            Header: 'Description',
            accessor: 'description'
          },
          {
            Header: 'Status',
            accessor: 'status'
          },
          {
            Header: 'Last Update',
            accessor: 'updateAt'
          },
          {
            Header: 'Action',
            Cell: row => {
              return (
                <div>
                  <button className='btn btn-xs btn-primary bg-slate-400'
                          onClick={() => this.handleViewToolDetails(row.original)}>Details
                  </button>
                </div>
              )
            }
          }
        ],
        toolTemplate: {
          columns: [
            {
              Header: 'Template',
              columns: [
                {
                  Header: 'ITEM',
                  accessor: 'name',
                  Cell: row => {
                    return <input
                      onChange={(e) => this.handleOnChangeInputName(e, row)}
                      disabled={!this.state.modal.isEdit}
                      value={row.original.name}/>
                  }
                },
                {
                  Header: 'ALIAS',
                  accessor: 'alias',
                  Cell: row => {
                    return <input
                      onChange={(e) => this.handleOnChangeInputAlias(e, row)}
                      disabled={!this.state.modal.isEdit}
                      value={row.original.alias}/>
                  }
                },
                {
                  Header: 'TYPE',
                  accessor: 'type',
                  Cell: row => {
                    return (
                      <Select
                        onChange={(e) => this.handleOnChangeInputType(e, row)}
                        isDisabled={!this.state.modal.isEdit}
                        value={{label: row.original.type, value: row.original.type}}
                        options={[
                          {label: 'text', value: 'text'},
                          {label: 'dropbox', value: 'dropbox'},
                          {label: 'file', value: 'file'},
                        ]}/>
                    )
                  }
                },
                {
                  Header: 'OPTIONS',
                  minWidth: 200,
                  accessor: 'options',
                  Cell: row => {
                    if (row.original.type === 'dropbox')
                      return (
                        <Creatable
                          onChange={(e) => this.handleOnChangeInputOptions(e, row)}
                          isDisabled={!this.state.modal.isEdit}
                          isMulti={true}
                          value={row.original.values}
                          options={row.original.values}/>
                      )
                  }
                },
                {
                  Header: 'DEFAULT',
                  accessor: 'defaultValue',
                  Cell: row => {
                    if (row.original.type === 'dropbox')
                      return (
                        <Select
                          onChange={(e) => this.handleOnChangeInputDefaultValue(e, row)}
                          isDisabled={!this.state.modal.isEdit}
                          isMulti={false}
                          options={this.state.modal.data.template[row.index].values}
                          value={{value: row.original.default, label: row.original.default}}/>
                      )
                  }
                },
                {
                  Header: 'DESCRIPTION',
                  accessor: 'description',
                  Cell: row => {
                    return (
                      <textarea
                        onChange={(e) => this.handleOnChangeInputDescription(e, row)}
                        className="form-control" value={row.original.value}
                        value={row.original.description}
                        disabled={!this.state.modal.isEdit}/>
                    )
                  }
                },
                {
                  Header: 'DELETE',
                  maxWidth: 70,
                  accessor: '',
                  Cell: row => {
                    return (
                      <button className="btn btn-danger text-center"
                              disabled={!this.state.modal.isEdit}
                              onClick={() => this.handleOnDeleteRow(row)}><i
                        className="icon-x"></i></button>
                    )
                  }
                },
              ]
            }
          ],
        }
      },
      modal: {
        isOpen: false,
        data: {
          template: [],
          name: '',
          description: ''
        }
      },
      isEdit: false
    }
  }

  render() {
    let collapsibleControlsGroup = '#collapsible-controls-group';
    let collapsibleControlsGroupElement = 'collapsible-controls-group';
    return (
      <div>
        <div className="navbar navbar-default header-highlight  sticky">
          <Header/>
        </div>
        <div className="page-container data">
          <div className="page-content">
            <SideBar/>
            <div className="content-wrapper">
              <div className="content">
                <div className="panel-group collapsible-sortable content-group-lg ui-sortable">
                  <div className="panel">
                    <div className="panel-heading bg-slate-800">
                      <div className="heading-elements">
                        <button type="button"
                                className="btn border-white text-white-800 btn-flat dropdown-toggle legitRipple"
                                onClick={() => this.handleOnAddNewTool()}>
                          ADD NEW <span className="icon-plus2"></span></button>
                      </div>
                    </div>
                    <div className="panel-body">
                      <ReactTable data={this.props.tools}
                                  columns={this.state.table.listTools}
                                  defaultPageSize={10}
                                  className="-striped -highlight"/>
                    </div>
                    <Modal
                      isOpen={this.state.modal.isOpen}
                      onRequestClose={this.handleCloseModal.bind(this)}
                      contentLabel={this.state.modal.data.name}
                      ariaHideApp={false}
                      shouldCloseOnEsc={true}
                      className='modal-dialog modal-lg'>

                      <div className="modal-content">
                        <div className="modal-header">
                          <button className="close icon-x"
                                  type="button"
                                  data-dismiss="modal"
                                  onClick={() => this.handleCloseModal()}/>
                        </div>

                        <div className="modal-body">
                          <div className="row mb-10">
                            <div className='form-group'>
                              <div className="col-md-4 text-left">
                                <label>Name: </label>
                                <input className='form-group ml-10'
                                       style={{width: "80%"}}
                                       disabled={!this.state.modal.isEdit}/>
                              </div>
                              <div className="col-md-8 text-left">
                                <label>Description: </label>
                                <input className='form-group ml-10'
                                       style={{width: "80%"}}
                                       disabled={!this.state.modal.isEdit}/>
                              </div>
                            </div>
                          </div>
                          <ReactTable data={this.state.modal.data.template}
                                      columns={this.state.table.toolTemplate.columns}
                                      defaultPageSize={this.state.modal.data.template.length ? this.state.modal.data.template.length : 5}
                                      showPagination={false}/>
                        </div>

                        {
                          this.state.modal.isEdit ?
                            (
                              <div className="modal-footer">
                                <button type="button" className="btn btn-xlg"
                                        onClick={() => this.handleOnAddRow()}><i className="icon-plus2"></i>
                                </button>
                                <button type="button" className="btn btn-danger btn-xlg"
                                        onClick={() => this.handleOnSaveModal()}>SAVE
                                </button>
                                <button type="button" className="btn btn-warning btn-xlg"
                                        onClick={() => this.handleOnCancelEditModal()}>CANCEL
                                </button>
                                <button type="button" className="btn btn-primary"
                                        data-dismiss="modal"
                                        onClick={() => this.handleCloseModal()}>CLOSE
                                </button>
                              </div>
                            )
                            :
                            (
                              <div className="modal-footer">
                                <button type="button" className="btn btn-default btn-xlg"
                                        onClick={this.handleSwitchToEditMode.bind(this)}>
                                  EDIT
                                </button>
                                <button type="button" className="btn btn-primary"
                                        data-dismiss="modal"
                                        onClick={this.handleCloseModal.bind(this)}>CLOSE
                                </button>
                              </div>
                            )
                        }
                      </div>
                    </Modal>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

    )
  }

  componentDidMount() {
    this.props.dispatch(fetchListTool())
  }

  handleViewToolDetails(tool) {
    this.handleOpenModal(tool)
  }

  handleCloseModal() {
    this.setState({
      ...this.state,
      modal: {
        ...this.state.modal,
        isOpen: false,
        data: {
          template: [],
          name: '',
          description: ''
        }
      }
    })
  }

  handleOpenModal(tool) {
    if (tool)
      this.setState({
        ...this.state,
        modal: {
          ...this.state.modal,
          isOpen: true,
          data: tool
        }
      })
  }

  handleSwitchToEditMode() {
    let tmpColumns = this.state.table.toolTemplate.columns[0].columns
    if (!tmpColumns.find(item => item.Header === 'DELETE'))
      tmpColumns.push({
        Header: 'DELETE',
        maxWidth: 70,
        accessor: '',
        Cell: row => {
          return (
            <button className="btn btn-danger text-center" onClick={() => this.handleOnDeleteRow(row)}><i
              className="icon-x"></i></button>
          )
        }
      })

    this.setState({
      ...this.state,
      table: {
        ...this.state.table,
        toolTemplate: {
          ...this.state.table.toolTemplate,
          columns: [
            {
              Header: 'Template',
              columns: tmpColumns
            }
          ]
        }
      },
      modal: {
        ...this.state.modal,
        isEdit: true
      }
    })
  }

  handleOnSaveModal() {

  }

  handleOnCancelEditModal() {
    this.setState({
      ...this.state,
      table: {
        ...this.state.table,
        toolTemplate: {
          ...this.state.table.toolTemplate,
        }
      },
      modal: {
        ...this.state.modal,
        isEdit: false
      }
    })
  }

  handleOnAddNewTool() {
    this.setState({
      ...this.state,
      modal: {
        ...this.state.modal,
        isEdit: true,
        isOpen: true,
        data: {
          template: [],
          name: '',
          description: ''
        }
      },
      table: {
        ...this.state.table,
        toolTemplate: {
          ...this.state.table.toolTemplate,
        }
      }
    })
  }

  handleOnDeleteRow(row) {
    let tmpTemplate = this.state.modal.data.template.splice(row.index, 1)
    this.setState({
      ...this.state,
      modal: {
        ...this.state.modal,
        data: {
          ...this.state.modal.data,
          template: tmpTemplate
        }
      }
    })
  }

  handleOnAddRow() {
    this.setState({
      ...this.state,
      modal: {
        ...this.state.modal,
        data: {
          ...this.state.modal.data,
          template: [
            ...this.state.modal.data.template,
            {name: '', alias: '', type: '', default: '', description: '', values: []}
          ]
        }
      }
    })
  }

  handleOnChangeInputName(e, row) {
    let tmpState = this.state
    tmpState.modal.data.template[row.index].name = e.target.value
    this.setState(tmpState)
  }

  handleOnChangeInputAlias(e, row) {
    let tmpState = this.state
    tmpState.modal.data.template[row.index].alias = e.target.value
    this.setState(tmpState)
  }

  handleOnChangeInputType(e, row) {
    let tmpState = this.state
    tmpState.modal.data.template[row.index].type = e.value
    this.setState(tmpState)
  }

  handleOnChangeInputOptions(e, row) {
    let tmpState = this.state
    tmpState.modal.data.template[row.index].values = e
    this.setState(tmpState)
  }

  handleOnChangeInputDefaultValue(e, row) {
    let tmpState = this.state
    tmpState.modal.data.template[row.index].default = e.value
    this.setState(tmpState)
  }

  handleOnChangeInputDescription(e, row) {
    let tmpState = this.state
    tmpState.modal.data.template[row.index].description = e.target.value
    this.setState(tmpState)
  }
}

const mapStateToProps = state => ({
  tools: state.tool.tools
})

export default connect(mapStateToProps)(ComponentAdminTool)