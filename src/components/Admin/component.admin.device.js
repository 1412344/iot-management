import {connect} from "react-redux";
import React, {Component} from 'react'
import ReactTable from 'react-table'
import Modal from 'react-modal'
import {toast} from 'react-toastify'

import Header from '../Layout/header'
import SideBar from '../Layout/sidebar'
import {addNewDevice, deleteDevice, fetchAllDeviceForAdmin, updateDeviceInfor} from "../../actions/admin";

class AdminDeviceManagement extends Component {
  constructor(props) {
    super(props)

    this.state = {
      table: {
        device: {
          columns: [
            {
              Header: 'Name',
              accessor: 'deviceName',
              width: 100
            },
            {
              Header: 'Details',
              accessor: 'detail',
              sortable: false,
              filterable: false
            },
            {
              Header: "Platform",
              accessor: 'platform'
            },
            {
              Header: 'Operating System',
              accessor: 'operating_system'
            },
            {
              Header: 'Action',
              filterable: false,
              sortable: false,
              width: 150,
              Cell: row => {
                return (
                  <div className="btn-group text-center">
                    <button className="btn btn-sm border-slate" onClick={(e) => this.handleOnViewDeviceDetails(e, row)}>
                      <b><i
                        className="icon-task"></i></b></button>
                    <button className="btn btn-sm border-slate" onClick={(e) => this.handleOnEditDeviceInfor(e, row)}>
                      <b><i className="icon-wrench2"></i></b></button>
                    <button className="btn btn-sm border-slate" onClick={(e) => this.handleOnDeleteDevice(e, row)}>
                      <b><i className="icon-x"></i></b></button>
                  </div>
                )
              }
            }
          ]
        }
      },
      modal: {
        isOpen: false,
        mode: 0, //0 - view; 1 - edit; 2 - add new
        data: {}
      }
    }
  }

  componentDidMount() {
    this.props.dispatch(fetchAllDeviceForAdmin())
  }

  render() {
    console.log("ADMIN.ACCOUNT STATE:", this.state)
    console.log("ADMIN.ACCOUNT PROPS:", this.props)
    return (
      <div>
        <div className="navbar navbar-default header-highlight  sticky">
          <Header/>
        </div>
        <div className="page-container data">
          <div className="page-content">
            <SideBar/>
            <div className="content-wrapper">
              <div className="content">
                <div className="panel-group collapsible-sortable content-group-lg ui-sortable">
                  <div className="panel">
                    <div className="panel-heading bg-slate-800">
                      <div className="heading-elements">
                        <button type="button"
                                className="btn border-white text-white-800 btn-flat dropdown-toggle legitRipple"
                                onClick={() => this.handleOnOpenAddNewDeviceModal()}>
                          ADD NEW <span className="icon-plus2"></span></button>
                      </div>
                    </div>
                    <div className="panel-body">
                      <ReactTable
                        filterable={true}
                        sortable={true}
                        columns={this.state.table.device.columns}
                        data={this.props.admin.device.items}/>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <Modal
          isOpen={this.state.modal.isOpen}
          ariaHideApp={false}
          className='modal-dialog modal-sm'
          shouldCloseOnEsc={true}
          onRequestClose={() => this.handleOnCloseModal()}>
          <div className="modal-content">
            <div className="modal-header">
              <h4>{this.state.modal.mode === 0 ? 'DEVICE DETAILS' : 'EDIT DEVICE INFORMATION'}</h4>
              <button className="close icon-x"
                      type="button"
                      data-dismiss="modal"
                      onClick={() => this.handleOnCloseModal()}/>
            </div>
            <div className="modal-body">
              <div className="table-responsive">
                <table className="table table-bordered">
                  <tbody>
                  <tr>
                    <th>Name</th>
                    {this.state.modal.mode < 2 ?
                      <th>{this.state.modal.data.deviceName}</th> :
                      <th><input className="form-control"
                                 value={this.state.modal.data.deviceName}
                                 onChange={(e) => this.handleOnChangeInputValue(e, 'deviceName')}/></th>}
                  </tr>
                  <tr>
                    <th>Platform</th>
                    {this.state.modal.mode < 2 ?
                      <th>{this.state.modal.data.platform}</th> :
                      <th><select className="form-control"
                                  onChange={(e) => this.handleOnChangeInputValue(e, 'platform')}>
                        <option value='arduino' selected={true}>Arduino</option>
                        <option value='raspberry'>Raspberry</option>
                      </select></th>}
                  </tr>
                  <tr>
                    <th>Details</th>
                    {this.state.modal.mode == 0 ?
                      <th>{this.state.modal.data.detail}</th> :
                      <th><input className="form-control"
                                 value={this.state.modal.data.detail}
                                 onChange={(e) => this.handleOnChangeInputValue(e, 'detail')}/></th>}
                  </tr>
                  <tr>
                    <th>Operatoring System</th>
                    {this.state.modal.mode == 0 ?
                      <th>{this.state.modal.data.operating_system}</th> :
                      <th><input className="form-control"
                                 value={this.state.modal.data.operating_system}
                                 onChange={(e) => this.handleOnChangeInputValue(e, 'operating_system')}/></th>}
                  </tr>
                  {
                    this.state.modal.mode < 1 ?
                      <tr>
                        <th>Last Update</th>
                        <th>{this.state.modal.data.checkStatusAt}</th>
                      </tr> :
                      null
                  }
                  </tbody>
                </table>
              </div>
            </div>
            {this.state.modal.mode == 0 ?
              (
                <div className="modal-footer">
                  {this.state.modal.data.platform == 'raspberry' ?
                    (
                      <button className="btn btn-warning"
                              onClick={() => this.handleOnOpenReInstallOsWindow(this.state.modal.data._id)}>
                        Redirect
                      </button>
                    ) : null}
                  <button className="btn btn-primary"
                          onClick={() => this.handleChangeToEditMode()}>EDIT
                  </button>
                  <button className="btn"
                          onClick={() => this.handleOnCloseModal()}>CANCEL
                  </button>
                </div>
              ) :
              this.state.modal.mode == 1 ?
                (
                  <div className="modal-footer">
                    <button className="btn btn-warning"
                            onClick={() => this.handleOnSaveDeviceInfor()}>SAVE
                    </button>
                    <button className="btn"
                            onClick={() => this.handleOnCloseModal()}>CANCEL
                    </button>
                  </div>
                ) :
                (
                  <div className="modal-footer">
                    <button className="btn btn-info"
                            onClick={() => this.handleOnAddNewDevice()}>ADD
                    </button>
                    <button className="btn"
                            onClick={() => this.handleOnCloseModal()}>CANCEL
                    </button>
                  </div>
                )}
          </div>
        </Modal>
      </div>
    )
  }

  handleOnViewDeviceDetails(e, row) {
    let tmpState = this.state
    tmpState.modal.isOpen = true
    tmpState.modal.data = row.original
    tmpState.modal.mode = 0
    this.setState(tmpState)
  }

  handleOnEditDeviceInfor(e, row) {
    let tmpState = this.state
    tmpState.modal.isOpen = true
    tmpState.modal.data = row.original
    tmpState.modal.mode = 1
    this.setState(tmpState)
  }

  handleOnDeleteDevice(e, row) {
    if (window.confirm(`Are you sure to delete DEVICE ${row.original.deviceName}?`)) {
      this.props.dispatch(deleteDevice(row.original._id))
    }
  }

  handleOnCloseModal() {
    let tmpState = this.state
    tmpState.modal.isOpen = false
    tmpState.modal.data = {}
    tmpState.modal.mode = 0
    this.setState(tmpState)
  }

  handleChangeToEditMode() {
    let tmpState = this.state
    tmpState.modal.mode = 1
    this.setState(tmpState)
  }

  handleChangeToViewMode() {
    let tmpState = this.state
    tmpState.modal.mode = 0
    this.setState(tmpState)
  }

  handleOnSaveDeviceInfor() {
    this.props.dispatch(updateDeviceInfor(this.state.modal.data))
    this.handleChangeToViewMode()
  }

  handleOnChangeInputValue(e, field) {
    let tmpState = this.state
    tmpState.modal.data[field] = e.target.value
    this.setState(tmpState)
  }

  handleOnOpenAddNewDeviceModal() {
    let tmpState = this.state
    tmpState.modal.isOpen = true
    tmpState.modal.data = {
      deviceName: '',
      detail: '',
      platform: 'arduino',
      operating_system: ''
    }
    tmpState.modal.mode = 2
    this.setState(tmpState)
  }

  handleOnAddNewDevice() {
    this.props.dispatch(addNewDevice(this.state.modal.data))
  }

  handleOnOpenReInstallOsWindow(deviceId) {
    window.open(`https://10.71.2.46/`, deviceId)
  }
}

const mapStateToProps = state => ({
  admin: state.admin
})

export default connect(mapStateToProps)(AdminDeviceManagement)