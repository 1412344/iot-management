import {connect} from "react-redux";
import React, {Component} from 'react'
import ReactTable from 'react-table'
import Modal from 'react-modal'
import {toast} from 'react-toastify'

import Header from '../Layout/header'
import SideBar from '../Layout/sidebar'
import {banAccount, disableAccount, editRole, fetchAllAccount} from "../../actions/action.account";

class AccountManagement extends Component {
  constructor(props) {
    super(props)
    this.state = {
      table: {
        account: {
          columns: [
            {
              Header: 'Name',
              accessor: 'displayName'
            },
            {
              Header: "Username",
              accessor: 'username'
            },
            {
              Header: 'Role',
              Cell: row => {
                return <div>{row.original.role.name}</div>
              }
            },
            {
              Header: 'Action',
              Cell: row => {
                return (
                  <div className="btn-group text-center">
                    {/*<button className="btn btn-sm border-slate" onClick={(e) => this.handleOnViewAccountDetail(e, row)}>*/}
                    {/*<b><i*/}
                    {/*className="icon-user-tie"></i></b></button>*/}
                    <button className="btn btn-sm border-slate"
                            title='Edit Role'
                            onClick={(e) => this.handleOnEditRoleAccount(e, row)}>
                      <b><i className="icon-user-tie"></i></b></button>
                    <button className="btn btn-sm border-slate"
                            title='Ban'
                            onClick={(e) => this.handleOnBanAccount(e, row)}>
                      <b><i className="icon-user-block"></i></b></button>
                    <button className="btn btn-sm border-slate"
                            title='Disable'
                            onClick={(e) => this.handleOnDisableAccount(e, row)}>
                      <b><i className="icon-user-lock"></i></b></button>
                    <button className="btn btn-sm border-slate"
                            title='Delete'
                            onClick={(e) => this.handleOnDeleteAccount(e, row)}>
                      <b><i className="icon-x"></i></b></button>
                  </div>
                )
              }
            }
          ]
        },
        modal: {
          mode: 0, //0 - none; 1 - edit; 2 - add new
          columns: [
            {
              Header: 'field',
              Cell: row => {
                return <div className="text-center">{row.original[0]}</div>
              },
            },
            {
              Header: 'value',
              Cell: row => {
                if (typeof row.original[1] === "object") {
                  return <input className="form-control" value={row.original[1].name.toString()}
                                onChange={(e) => this.handleOnChangeInput(e, row)}/>
                } else
                  return <input className="form-control" value={row.original[1].toString()}
                                onChange={(e) => this.handleOnChangeInput(e, row)}/>
              }
            }
          ]
        }
      },
      modal: {
        isOpen: false,
        edit: {
          data: {}
        },
        add: {
          isOpen: false,
          data: {}
        }
      }
    }
  }

  componentDidMount() {
    this.props.dispatch(fetchAllAccount())
  }

  render() {
    console.log('ADMIN ACCOUNT STATE', this.state)
    return (
      <div>
        <div className="navbar navbar-default header-highlight  sticky">
          <Header/>
        </div>
        <div className="page-container data">
          <div className="page-content">
            <SideBar/>
            <div className="content-wrapper">
              <div className="content">
                <div className="panel-group collapsible-sortable content-group-lg ui-sortable">
                  <div className="panel">
                    <div className="panel-heading bg-slate-800">
                      {/*<div className="heading-elements">*/}
                        {/*<button type="button"*/}
                                {/*className="btn border-white text-white-800 btn-flat dropdown-toggle legitRipple"*/}
                                {/*onClick={() => this.handleOnAddNewAccount()}>*/}
                          {/*ADD NEW <span className="icon-plus2"></span></button>*/}
                      {/*</div>*/}
                    </div>
                    <div className="panel-body">
                      <ReactTable
                        sortable={false}
                        columns={this.state.table.account.columns}
                        // defaultPageSize={this.props.account.items.length}
                        data={this.props.admin.account.items}/>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <Modal
          isOpen={this.state.modal.isOpen}
          ariaHideApp={false}
          className='modal-dialog modal-sm'
          shouldCloseOnEsc={true}
          onRequestClose={() => this.handleOnCloseModal()}>
          <div className="modal-content">
            <div className="modal-header">
              <h5>EDIT ACCOUNT ROLE</h5>
              <button className="close icon-x"
                      type="button"
                      data-dismiss="modal"
                      onClick={() => this.handleOnCloseModal()}/>
            </div>
            <div className="modal-body">
              <div className="form-horizontal">
                <div className="form-group">
                  <label className="col-md-2 control-label">Name</label>
                  <div className="col-md-10">
                    <input className="form-control" disabled={true} value={this.state.modal.edit.data.displayName}/>
                  </div>
                </div>
                <div className="form-group">
                  <label className="col-md-2 control-label">Username</label>
                  <div className="col-md-10">
                    <input className="form-control" disabled={true} value={this.state.modal.edit.data.username}/>
                  </div>
                </div>
                <div className="form-group">
                  <label className="col-md-2 control-label">Role</label>
                  <div className="col-md-10">
                    <select className='form-control'
                            value={this.state.modal.edit.data.role ? this.state.modal.edit.data.role.name : 'guest'}
                            onChange={(e) => this.handleOnChangeInput(e, 'role')}>
                      <option className='form-control' value="student">Student</option>
                      <option className='form-control' value="supporter">Supporter</option>
                      <option className='form-control' value="teacher">Teacher</option>
                      <option className='form-control' value="guest">Guest</option>
                    </select>
                  </div>
                </div>
                {/*<div className="form-group">*/}
                {/*<label className="col-md-2 control-label">Class</label>*/}
                {/*<div className="col-md-10">*/}
                {/*<input className="form-control" value={this.state.modal.edit.data.class}/>*/}
                {/*</div>*/}
                {/*</div>*/}
                {/*<div className="form-group">*/}
                {/*<label className="col-md-2 control-label">Facility</label>*/}
                {/*<div className="col-md-10">*/}
                {/*<input className="form-control" value={this.state.modal.edit.data.facility}/>*/}
                {/*</div>*/}
                {/*</div>*/}
                {/*<div className="form-group">*/}
                {/*<label className="col-md-2 control-label">University</label>*/}
                {/*<div className="col-md-10">*/}
                {/*<input className="form-control" value={this.state.modal.edit.data.university}/>*/}
                {/*</div>*/}
                {/*</div>*/}
                {/*<div className="form-group">*/}
                {/*<label className="col-md-2 control-label">Phone</label>*/}
                {/*<div className="col-md-10">*/}
                {/*<input className="form-control" value={this.state.modal.edit.data.phone}/>*/}
                {/*</div>*/}
                {/*</div>*/}
              </div>
            </div>
            <div className="modal-footer">
              <button className="btn btn-warning"
                      onClick={(e) => this.handleOnEditRole(e)}>SAVE
              </button>
              <button className="btn"
                      onClick={(e) => this.handleOnCancelEditAccount(e)}>CANCEL
              </button>
            </div>
          </div>
        </Modal>
      </div>
    )
  }

  handleOnViewAccountDetail(e, row) {
    this.handleOnOpenModal()
    return undefined;
  }

  handleOnEditRoleAccount(e, row) {
    let tmpState = this.state
    tmpState.modal.edit.data = row.original
    tmpState.modal.isOpen = true
    this.setState(tmpState)
    return undefined;
  }

  handleOnDeleteAccount(e, row) {
    return undefined;
  }

  handleOnCloseModal() {
    let tmpState = this.state
    tmpState.modal.isOpen = false
    this.setState(tmpState)
  }

  handleOnOpenModal() {
    let tmpState = this.state
    tmpState.modal.isOpen = true
    this.setState(tmpState)
  }

  handleOnDisableAccount(e, row) {
    toast.warn('DISABLE ACCOUNT: ' + row.original.displayName)
    this.props.dispatch(disableAccount(row.original.email))
  }

  handleOnChangeInput(e, field) {
    let tmpState = this.state
    tmpState.modal.edit.data[field] = e.target.value
    this.setState(tmpState)
  }

  handleOnEditRole(e) {
    this.props.dispatch(editRole(this.state.modal.edit.data._id, this.state.modal.edit.data.role))
  }

  handleOnCancelEditAccount(e) {
    this.handleOnCloseModal()
  }

  handleOnBanAccount(e, row) {
    this.props.dispatch(banAccount(row.original.email))
  }

  handleOnAddNewAccount() {
    let tmpState = this.state
    tmpState.modal.add.isOpen = true
    tmpState.modal.add.data = {
      username: '',
      password: '',
      role: 'guest'
    }
  }
}

const mapStateToProps = state => ({
  // account: state.account,
  admin: state.admin
})

export default connect(mapStateToProps)(AccountManagement)