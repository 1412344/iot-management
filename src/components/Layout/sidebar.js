import {connect} from "react-redux";
import React, {Component} from 'react'
import {Link} from 'react-router'

class SideBar extends Component {
  constructor(props) {
    super(props)
    this.state = {}
  }

  componentDidMount() {
    //   // Hide all nested lists
    //   $('.navigation').find('li').not('.active, .category-title').has('ul').children('ul').addClass('hidden-ul');
    //
    //   // Highlight children links
    //   $('.navigation').find('li').has('ul').children('a').addClass('has-ul');
    //
    //   // Add active state to all dropdown parent levels
    //   $('.dropdown-menu:not(.dropdown-content), .dropdown-menu:not(.dropdown-content) .dropdown-submenu').has('li.active').addClass('active').parents('.navbar-nav .dropdown:not(.language-switch), .navbar-nav .dropup:not(.language-switch)').addClass('active');
    //
    //   $('.navigation-main').find('li').has('ul').children('a').on('click', function (e) {
    //     e.preventDefault();
    //
    //     // Collapsible
    //     $(this).parent('li').not('.disabled').not($('.sidebar-xs').not('.sidebar-xs-indicator').find('.navigation-main').children('li')).toggleClass('active').children('ul').slideToggle(250);
    //
    //     // Accordion
    //     if ($('.navigation-main').hasClass('navigation-accordion')) {
    //       $(this).parent('li').not('.disabled').not($('.sidebar-xs').not('.sidebar-xs-indicator').find('.navigation-main').children('li')).siblings(':has(.has-ul)').removeClass('active').children('ul').slideUp(250);
    //     }
    //   });
  }

  render() {
    const {user, dispatch} = this.props
    return (
        <div className="sidebar sidebar-main">
          <div className="sidebar-content">

            <div className="sidebar-user-material">
              <div className="category-content">
                <div className="sidebar-user-material-content">
                  <Link to=""><img src="/src/Linkssets/images/placeholder.jpg" className="img-circle img-responsive"
                                   alt=""/></Link>
                  <div className="text-center"><h1></h1></div>

                </div>
              </div>
            </div>

            <div className="sidebar-category sidebar-category-visible">
              <div className="category-content no-padding">
                <ul className="navigation navigation-main navigation-accordion">

                  <li className="active"><Link to="/dashboard"><i className="icon-home4"></i>
                    <span>Dashboard</span></Link></li>
                  <li>
                    <Link to="#"><i className="icon-user"></i> <span>My Account</span></Link>
                    <ul>
                      <li><Link to="/user"><i className="icon-user-plus"></i>
                        <span>My profile</span></Link></li>
                      <li className="divider"></li>
                      <li><Link to="/logout"><i className="icon-switch2"></i> <span>Logout</span></Link>
                      </li>
                    </ul>
                  </li>
                  <li>
                    <Link to="/device/manage"><i className="icon-cog2"></i> <span>Device</span></Link>
                    <ul>
                      <li><Link to="/device/assign"><i className="icon-list"></i>
                        <span>Assign New</span></Link></li>
                      <li className="divider"></li>
                      <li><Link to="/device/manage"><i className="icon-markup"></i>
                        <span>Manage</span></Link>
                      </li>
                    </ul>
                  </li>
                  {
                    user.role === 'teacher' || user.role === 'supporter' || user.role === 'admin' ?
                        (
                            <li>
                              <Link to="#"><i className="icon-user"></i> <span>Admin</span></Link>
                              <ul>
                                <li><Link to="/admin/account"><i className="icon-user-plus"></i>
                                  <span>Account</span></Link></li>
                                <li className="divider"></li>
                                <li><Link to="/admin/device"><i className="icon-switch2"></i>
                                  <span>Device</span></Link>
                                </li>
                              </ul>
                            </li>
                        )
                        :
                        (null)
                  }
                </ul>
              </div>
            </div>
          </div>
        </div>
    )
  }
}


const mapStateToProps = state => {
  return {
    user: state.auth.user
  }
}

export default connect(mapStateToProps)(SideBar)