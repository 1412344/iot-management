import {connect} from "react-redux";
import React, {Component} from 'react'
import {ToastContainer} from 'react-toastify'
import Socket from '../socket'
import {updateUserDataFromLocalStorage} from "../../actions/user";
import {browserHistory} from 'react-router'

class Header extends Component {
  constructor(props) {
    super(props)
  }

  componentDidMoutn() {
    if (Object.keys(this.props.user).length === 0 && this.props.user.constructor === Object)
      this.props.dispatch(updateUserDataFromLocalStorage())
  }

  render() {
    let {items, unread, user} = this.props
    if (!user) {
      return browserHistory.push('/login')
    }
    else
      return (
        <div className="Header">
          <ToastContainer/>
          <Socket/>
          <div className="navbar-header">
            <a className="navbar-brand" href="/Dashboard"></a>
            <ul className="nav navbar-nav visible-xs-block">
              <li><a data-toggle="collapse" data-target="#navbar-mobile"><i className="icon-tree5"></i></a>
              </li>
              <li><a className="sidebar-mobile-main-toggle"><i className="icon-paragraph-justify3"></i></a>
              </li>
            </ul>
          </div>

          <div className="navbar-collapse collapse" id="navbar-mobile">
            <ul className="nav navbar-nav">
              <li><a className="sidebar-control sidebar-main-toggle hidden-xs"><i
                className="icon-paragraph-justify3"></i></a></li>
            </ul>
            <div className="navbar-right">
              <ul className="nav navbar-nav">
                <li className="dropdown dropdown-user">
                  <a className="dropdown-toggle" data-toggle="dropdown">
                    <span>{user.username}</span>
                    <i className="caret"></i>
                  </a>
                  <ul className="dropdown-menu dropdown-menu-right">
                    <li>
                      <a href='/user'>Your Profile</a>
                    </li>
                    <li>
                      <a href='/logout'>Logout</a>
                    </li>
                  </ul>
                </li>
              </ul>
            </div>
          </div>
        </div>
      )
  }
}

const mapStateToProps = state => ({
  user: state.auth.user
})

export default connect(mapStateToProps)(Header)