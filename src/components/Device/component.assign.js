import {connect} from "react-redux";
import React, {Component} from 'react'
import ReactTable from 'react-table'
import Modal from 'react-modal'
import {toast} from 'react-toastify'

import Header from '../Layout/header'
import SideBar from '../Layout/sidebar'
import {borrowDevice, fetchListFreeDevices} from "../../actions/device";

class AssignDevice extends Component {
  constructor(props) {
    super(props);

    this.state = {
      table: {
        device: {
          columns: [
            {
              Header: 'Name',
              accessor: 'deviceName',
              minWidth: 100,
              maxWidth: 100,
            },
            {
              Header: 'Detail',
              filterable: false,
              accessor: 'detail',
              style:{ 'white-space': 'unset'},
              minWidth: 100,
              // maxWidth: 500,
            },
            {
              Header: "Platform",
              accessor: 'platform',
              minWidth: 100,
              maxWidth: 200,
            },
            {
              Header: 'Operating System',
              accessor: 'operating_system',
              minWidth: 100,
              maxWidth: 200,
            },
            {
              Header: 'Action',
              minWidth: 100,
              maxWidth: 100,
              filterable: false,
              Cell: row => {
                return (
                  <div className="btn-group text-center">
                    <button className="btn btn-sm border-slate"
                            onClick={(e) => this.handleOnSelectTimesBorrowDevice(e, row)}>
                      BORROW
                    </button>
                  </div>
                )
              }
            }
          ]
        }
      },
      modal: {
        borrow: {
          isOpen: false,
          data: {
            deviceId: '',
            selectTimes: 5
          }
        },
      }
    }
  }

  componentDidMount() {
    this.props.dispatch(fetchListFreeDevices('null'))
  }

  render() {
    console.log('width', window.document.getElementsByClassName('ReactTable'))
    return (
      <div>
        <div className="navbar navbar-default header-highlight  sticky">
          <Header/>
        </div>
        <div className="page-container data">
          <div className="page-content">
            <SideBar/>
            <div className="content-wrapper">
              <div className="content">
                <div className="panel-group collapsible-sortable content-group-lg ui-sortable">
                  <div className="panel">
                    <div className="panel-body">
                      <ReactTable
                        filterable={true}
                        sortable={true}
                        columns={this.state.table.device.columns}
                        data={this.props.device.free.items}/>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <Modal
          isOpen={this.state.modal.borrow.isOpen}
          onRequestClose={() => this.handleOnCloseBorrowModal()}
          ariaHideApp={false}
          shouldCloseOnEsc={true}
          className='modal-dialog modal-sm'>
          <div className="modal-content">
            <div className="modal-header">
              <h4>SELECT BORROW TIME</h4>
              <button className="close icon-x"
                      type="button"
                      data-dismiss="modal"
                      onClick={() => this.handleOnCloseBorrowModal()}/>
            </div>
            <div className="modal-body">
              <div className="row">
                <div className="col-md-4">
                  <p>Borrow Time</p>
                </div>
                <div className="col-md-8">
                  <input type='number'
                         value={this.state.modal.borrow.data.selectTimes}
                         ref="borrowTime"
                         className="form-control"
                         placeholder="In minute"
                         onChange={(e) => this.handleOnChangeSelectTime(e)}/>
                </div>
              </div>
            </div>
            <div className="modal-footer">
              <button className="btn btn-primary"
                      onClick={() => this.handleOnBorrowDevice()}>Borrow
              </button>
              <button className="btn"
                      onClick={() => this.handleOnCloseBorrowModal()}>Cancel
              </button>
            </div>
          </div>
        </Modal>
      </div>
    )
  }

  handleOnSelectTimesBorrowDevice(e, row) {
    this.handleOpenBorrowModal(row.original._id)
  }

  handleOpenBorrowModal(deviceId) {
    let tmpState = this.state
    tmpState.modal.borrow.isOpen = true
    tmpState.modal.borrow.data.deviceId = deviceId
    this.setState(tmpState)
  }

  handleOnCloseBorrowModal() {
    let tmpState = this.state
    tmpState.modal.borrow.isOpen = false
    this.setState(tmpState)
  }

  handleOnBorrowDevice() {
    this.props.dispatch(borrowDevice(this.state.modal.borrow.data.deviceId, this.state.modal.borrow.data.selectTimes))
    this.handleOnCloseBorrowModal()
  }

  handleOnChangeSelectTime(e) {
    let tmpState = this.state
    tmpState.modal.borrow.data.selectTimes = e.target.value
    this.setState(tmpState)
  }
}

const mapStateToProps = state => ({
  device: state.device
})

export default connect(mapStateToProps)(AssignDevice)