import {connect} from "react-redux";
import React, {Component} from 'react'
import {Controlled, UnControlled} from 'react-codemirror2'
import {toast} from 'react-toastify'

import Header from '../Layout/header'
import SideBar from '../Layout/sidebar'
import Tabs from "../CustomTab/component.Tabs";
import {deployCode, deployFile} from "../../actions/device";
import {deployCodeToIo} from "../../actions/socket";

require('codemirror/lib/codemirror.css');
require('codemirror/theme/material.css');
require('codemirror/theme/neat.css');
require('codemirror/mode/xml/xml.js');
require('codemirror/mode/javascript/javascript.js');

class DeviceInteract extends Component {
  constructor(props) {
    super(props)

    this.state = {
      // info: props.device.interact.items.find(i => i._id === this.props.device.interact.current)
      info: {
        "_id": "5c031f241e16a82d6c6d8090",
        "deviceName": "rasp2",
        "platform": "raspberry",
        "detail": "mô tả chi tiết về thiết bị raspberry pi 2",
        "checkStatusAt": "2018-12-03T19:55:25.733Z",
        "ssh_account": "rasp2",
        "ssh_password": "rasp2_password",
        "operating_system": "centos 6"
      },
      file: null,
      code: {
        value: `int ledPin = 2;
int i=0;
// the setup function runs once when you press reset or power the board
void setup() {
  Serial.begin(115200);
  // initialize digital pin LED_BUILTIN as an output.
  pinMode(ledPin, OUTPUT);
}

// the loop function runs over and over again forever
void loop() {
  Serial.print("Hello ");
  Serial.print(i++);
  Serial.println();
  digitalWrite(ledPin, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay(50);                       // wait for a second
  digitalWrite(ledPin, LOW);    // turn the LED off by making the voltage LOW
  delay(50);                       // wait for a second
}`,
        options: {
          mode: 'javascript',
          theme: 'material',
          lineNumbers: 'true'
        }
      },
      result: {
        value: '',
        options: {
          mode: 'javascript',
          theme: 'material',
          lineNumbers: false
        }
      }
    }
  }

  scrollToBottom = () => {
    if (this.resultEnd) {
      this.resultEnd.scrollTop = this.resultEnd.scrollHeight;
    }
  };

  componentDidMount() {
    this.scrollToBottom();
  }

  componentDidUpdate() {
    this.scrollToBottom();
  }

  render() {
    const result = this.props.device.interact.result.find(i => i.deviceId === this.props.device.interact.current)
    return (
      <div>
        <div className="navbar navbar-default header-highlight sticky">
          <Header/>
        </div>
        <div className="page-container data">
          <div className="page-content">
            <SideBar/>
            <div className="content-wrapper">
              <div className="content">
                <div className="panel-group collapsible-sortable content-group-lg ui-sortable">
                  <div className="panel">
                    {/*<div className="panel-heading bg-slate-800">*/}
                    {/*<div className="heading-elements">*/}
                    {/*</div>*/}
                    {/*</div>*/}
                    <div className="panel-body">
                      <div className="row">
                        <div className="col-md-6">
                          <fieldset>
                            <legend>
                              <i className="icon-code mr-20"></i>
                              <b>CODE</b>
                            </legend>
                            <Tabs>
                              <div label="Upload">
                                <label>Upload file .ino</label>
                                <input type="file"
                                       className="form-control"
                                       onChange={(e) => this.handleOnChangeFile(e.target.files)}/>
                                <button className="btn btn-primary bg-slate mt-5"
                                        style={{width: '100%'}}
                                        onClick={() => this.handleOnUploadInoFile()}>UPLOAD AND DEPLOY
                                </button>
                              </div>
                              <div label="Code">
                                <Controlled
                                  value={this.state.code.value}
                                  options={this.state.code.options}
                                  onBeforeChange={(editor, data, value) => {
                                    this.setState({
                                      ...this.state,
                                      code: {
                                        ...this.state.code,
                                        value: value
                                      }
                                    });
                                  }}
                                  onChange={(editor, data, value) => {
                                  }}/>
                                <button className="btn btn-primary bg-slate mt-5"
                                        style={{width: '100%'}}
                                        onClick={() => this.handleDeployCode(this.props.socket.connection)}>DEPLOY
                                </button>
                              </div>
                            </Tabs>
                          </fieldset>
                        </div>
                        <div className="col-md-6">
                          <fieldset>
                            <legend>
                              <i className="icon-book-play mr-20"></i>
                              <b>RESULT</b>
                            </legend>
                            <textarea
                              autoComplete="off"
                              autoCorrect="off"
                              autoCapitalize="off"
                              spellCheck="false"
                              ref={el => {
                                this.resultEnd = el
                              }}
                              readOnly={true}
                              style={{width: '100%', height: '400px'}}
                              value={result ? result.result : ''}
                              className="bg-slate"></textarea>
                          </fieldset>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }

  handleDeployCode() {
    if(this.state.code.value.length)
      this.props.dispatch(deployCode(this.props.socket.connection.id, this.props.device.interact.current, this.state.code.value))
    else
      toast.warn('Code must not empty')
    // this.props.dispatch(deployCodeToIo(socket, this.props.device.interact.current, this.state.code.value))
  }

  handleOnUploadInoFile() {
    if (this.state.file) {
      let formData = new FormData()
      formData.append('file', this.state.file[0])
      formData.append('socketId', this.props.socket.connection.id)
      formData.append('deviceId', this.props.device.interact.current)
      this.props.dispatch(deployFile(formData))
    } else {
      toast.warn('File must not empty')
    }
  }

  handleOnChangeFile(selectorFiles) {
    let tmpState = this.state
    tmpState.file = selectorFiles
    this.setState(tmpState)
  }
}

const mapStateToProps = state => ({
  device: state.device,
  socket: state.socket
})

export default connect(mapStateToProps)(DeviceInteract)