import {connect} from "react-redux";
import React, {Component} from 'react'
import ReactTable from 'react-table'
import Modal from 'react-modal'
import {toast} from 'react-toastify'

import Header from '../Layout/header'
import SideBar from '../Layout/sidebar'
import {
  fetchListBorrowDevice, fetchListFreeDevices, interactWithDevice, removeDevice,
  revokeDevice
} from "../../actions/device";
import ProgressBar from "../progressBar";
import StickyBar from '../Layout/sitckyBar'

class Device extends Component {
  constructor(props) {
    super(props);

    this.state = {
      table: {
        device: {
          columns: [
            {
              Header: 'Name',
              maxWidth: 100,
              accessor: 'device.deviceName'
              // Cell: row => {
              //   return (
              //     <div className='text-center'>{row.original.device.deviceName}</div>
              //   )
              // }
            },
            // {
            //   Header: 'Detail',
            //   accessor: 'device.detail',
            //   filterable: false,
            //   style: {'whiteSpace': 'unset'}
            // },
            {
              Header: "Platform",
              accessor: 'device.platform',
              maxWidth: 70
            },
            {
              Header: 'Operating System',
              accessor: 'device.operating_system'
            },
            {
              Header: 'Expired At',
              filterable: false,
              Cell: row => {
                return (
                  <div>
                    <ProgressBar
                      removeExpiredItem={() => this.removeExpiredItem()}
                      deviceId={row.original._id}
                      startedAt={new Date(row.original.startedAt)}
                      expiredAt={new Date(row.original.expiredAt)}></ProgressBar>
                  </div>
                )
              }
            },
            {
              Header: 'Action',
              maxWidth: 300,
              filterable: false,
              Cell: row => {
                return (
                  <div className="btn-group text-center">
                    <button className="btn btn-sm border-slate"
                            onClick={(e) => this.handleOnViewDeviceDetails(e, row)}>
                      DETAILS
                    </button>
                    <button className="btn btn-sm border-slate"
                            onClick={(e) => this.handleOnRemoveBorrowedDevice(row.original._id)}>
                      REMOVE
                    </button>
                    {row.original.device.platform == 'arduino' ?
                      (
                        <button className="btn btn-sm border-slate"
                                style={{width: 100}}
                                onClick={(e) => this.handleInteractWithDevice(e, row)}>
                          INTERACT
                        </button>
                      ) : (
                        <button className="btn btn-sm border-slate"
                                style={{width: 100}}
                                onClick={(e) => this.handleReConfigRasDevice(e, row)}>
                          RE-CONFIG
                        </button>
                      )}
                  </div>
                )
              }
            }
          ]
        }
      },
      modal: {
        view: {
          isOpen: false,
          type: 'raspberry',
          data: {}
        },
        interact: {
          isOpen: false,
          data: {}
        }
      }
    }
  }

  componentDidMount() {
    this.props.dispatch(fetchListBorrowDevice(this.props.user.uid))
  }

  render() {
    return (
      <div>
        <div className="navbar navbar-default header-highlight  sticky">
          <Header/>
        </div>
        <div className="page-container data">
          <div className="page-content">
            <SideBar/>
            <div className="content-wrapper">
              <div className="content">
                <div className="panel-group collapsible-sortable content-group-lg ui-sortable">
                  <div className="panel">
                    <div className="panel-body">
                      <ReactTable
                        filterable={true}
                        sortable={true}
                        columns={this.state.table.device.columns}
                        data={this.props.device.borrow.items}/>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <Modal
          isOpen={this.state.modal.view.isOpen}
          onRequestClose={() => this.handleOnCloseViewDetailsModal()}
          className='modal-lg'
          ariaHideApp={false}
          shouldCloseOnEsc={true}
          className='modal-dialog modal-sm'>
          <div className="modal-content">
            <div className="modal-header">
              <h4>DEVICE DETAILS</h4>
              <button className="close icon-x"
                      type="button"
                      data-dismiss="modal"
                      onClick={() => this.handleOnCloseViewDetailsModal()}/>
            </div>

            <div className="modal-body">
              <div className="table-responsive">
                <table className="table table-striped table-bordered">
                  <tbody>
                  <tr>
                    <th>Name</th>
                    <th>{this.state.modal.view.data.deviceName}</th>
                  </tr>
                  <tr>
                    <th>Platform</th>
                    <th>{this.state.modal.view.data.platform}</th>
                  </tr>
                  <tr>
                    <th>Details</th>
                    <th>{this.state.modal.view.data.detail}</th>
                  </tr>
                  <tr>
                    <th>Last Update</th>
                    <th>{this.state.modal.view.data.checkStatusAt}</th>
                  </tr>
                  <tr>
                    <th>Operatoring System</th>
                    <th>{this.state.modal.view.data.operating_system}</th>
                  </tr>
                  {
                    this.state.modal.view.data.platform === 'raspberry' ? (
                      <tr>
                        <th>SSH Account</th>
                        <th>{this.state.modal.view.data.ssh_account}</th>
                      </tr>
                    ) : null
                  }
                  {
                    this.state.modal.view.data.platform === 'raspberry' ? (
                      <tr>
                        <th>SSH Password</th>
                        <th>{this.state.modal.view.data.ssh_password}</th>
                      </tr>
                    ) : null
                  }
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </Modal>
        <StickyBar/>
      </div>
    );
  }

  handleOnCloseViewDetailsModal() {
    let tmpState = this.state
    tmpState.modal.view.isOpen = false
    this.setState(tmpState)
  }

  handleInteractWithDevice(e, row) {
    this.props.dispatch(interactWithDevice(row.original.device._id))
  }

  handleReConfigRasDevice(e, row) {

  }

  handleOnViewDeviceDetails(e, row) {
    let tmpState = this.state
    tmpState.modal.view.isOpen = true
    tmpState.modal.view.data = row.original.device
    this.setState(tmpState)
  }

  removeExpiredItem(deviceId) {
    console.log('ITEM EXPIRED:', deviceId)
    this.props.dispatch(removeDevice(deviceId, 'interact'))
    this.props.dispatch(removeDevice(deviceId, 'borrow'))
  }

  handleOnRemoveBorrowedDevice(deviceId) {
    this.props.dispatch(revokeDevice(deviceId))
  }
}

const mapStateToProps = state => ({
  user: state.auth.user,
  device: state.device
})

export default connect(mapStateToProps)(Device)