export const SUBMIT_LOGIN_FORM = 'SUBMIT_LOGIN_FORM'
export const SUBMIT_LOGIN_FORM_SUCCESS = 'SUBMIT_LOGIN_FORM_SUCCESS'
export const SUBMIT_LOGIN_FORM_ERROR = 'SUBMIT_LOGIN_FORM_ERROR'

export const UPDATE_USER_TOKEN = 'UPDATE_USER_TOKEN'
export const UPDATE_USER_DATA_FROM_LOCAL_STORAGE = 'UPDATE_USER_DATA_FROM_LOCAL_STORAGE'

export const REGISTER_SUCCESS = 'REGISTER_SUCCESS'

export const LOGOUT = 'LOGOUT'